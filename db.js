const pgp = require('pg-promise')();

let state = {
	db:null,
};

exports.connect = (config, pgOption, done) => {
	if(state.db)
		return done();
	state.db = pgp({...config, ...pgOption});
	done();
};

exports.get = () => {return state.db};