const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const {config} = require('./config');
const db = require('./db');
const {check, cookie, validationResult} = require('express-validator/check');
const Authenticator = require('./base/Authenticator');

const Event = require('./models/Event');

const app = express();
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const tokens = new Map(); // токены авторизации (токен: Дата истечения)

app.post('/api/auth', async (req, res) => {
	try {
		const authData = await Authenticator.authenticate(req.body.login, req.body.password);
		res.cookie('auth_token', authData.token);
		res.cookie('identity', JSON.stringify(authData.identity));
		const expireDate = new Date();
		expireDate.setHours(expireDate.getHours() + 12);
		tokens.set(authData.token, expireDate);
		res.status(200).send();
	} catch (e) {
		if (e.code) {
			res.status(e.code).send(e.message);
		} else {
			console.error('Ошибка в ходе аутентификации', e);
			res.status(501).end();
		}
	}
});
app.get('/api/events',
	[
		cookie('authorization').custom(async (value, {req}) => {
			await checkAuthorization(req.cookies.auth_token, req.cookies.identity);
		}),
	],
	async (req, res) => {
		try {
			if (!validationResult(req).isEmpty()) {
				return res.status(401).json({errors: validationResult(req).array()});
			}
			const events = await Event.list();
			res.status(200).json(events);
		} catch (e) {
			if (e.code) {
				res.status(e.code).send(e.message);
			} else {
				console.error('Ошибка в ходе получения списка событий', e);
				res.status(501).end();
			}
		}
	});
app.get('/api/events/:id',
	[
		cookie('authorization').custom(async (value, {req}) => {
			await checkAuthorization(req.cookies.auth_token, req.cookies.identity);
		}),
	],
	async (req, res) => {
		try {
			if (!validationResult(req).isEmpty()) {
				return res.status(401).json({errors: validationResult(req).array()});
			}
			const event = await Event.getById(req.params.id);
			res.status(200).json(event);
		} catch (e) {
			if (e.code) {
				res.status(e.code).send(e.message);
			} else {
				console.error(`Ошибка в ходе получения события с id = ${req.params.id}`, e);
				res.status(501).end();
			}
		}
	});
app.post(
	'/api/events',
	[
		check('name').isLength({min: 3, max: 32}),
		check('name').isAscii(),
		check('start_date').exists(),
		check('end_date').exists(),
		cookie('authorization').custom(async (value, {req}) => {
			await checkAuthorization(req.cookies.auth_token, req.cookies.identity);
		}),
	],
	async (req, res) => {
		try {
			if (!validationResult(req).isEmpty()) {
				return res.status(401).json({errors: validationResult(req).array()});
			}
			const event = await new Event(req.body).save();
			res.status(200).json(event);
		} catch (e) {
			if (e.code) {
				res.status(e.code).send(e.message);
			} else {
				console.error('Ошибка в ходе создания нового события', e);
				res.status(501).end();
			}
		}
	});
app.patch(
	'/api/events/:id',
	[
		check('name').isLength({min: 3, max: 32}).optional(),
		check('name').isAscii().optional(),
		cookie('authorization').custom(async (value, {req}) => {
			await checkAuthorization(req.cookies.auth_token, req.cookies.identity);
		}),
	],
	async (req, res) => {
		try {
			if (!validationResult(req).isEmpty()) {
				return res.status(401).json({errors: validationResult(req).array()});
			}
			const event = await Event.update(req.params, req.body);
			res.status(200).json(event);
		} catch (e) {
			if (e.code) {
				res.status(e.code).send(e.message);
			} else {
				console.error(`Ошибка в ходе обновления события с id = ${req.params.id}`, e);
				res.status(501).end();
			}
		}
	});


db.connect(
	config.db,
	config.pgOption,
	() => app.listen(
		config.port,
		() => {
			console.log('test server start');
			setTimeout(deleteOldTokens, 10 * 60 * 1000); // Очищаем токены каждые 10 минут
		},
	)
);

function deleteOldTokens() {
	for (const pairs of tokens.entries()) {
		if (pairs[1] <= new Date) {
			tokens.delete(pairs[0]);
		}
	}
}

async function checkAuthorization(auth_token, identity) {
	const existToken = tokens.get(auth_token);
	await Authenticator.check(identity, auth_token, existToken);
}

app.use(function (err, req, res, next) {
	console.error('Общая ошибка', err.stack);
	if (err.code) {
		res.status(err.code).send(err.message);
		return;
	}
	res.status(500).send('Something broke!');
});
