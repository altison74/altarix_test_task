exports.config  = {
	port: 7234,
	pgOption:{
		query(e) {
			console.log('QUERY:', e.query);
		}
	},
	db: {
		host: 'localhost',
		port: 5432,
		database: 'test_task',
		user: 'postgres',
		password: '3v205c99',
	}
};