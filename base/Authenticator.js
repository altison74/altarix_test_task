const db = require('../db');
const CodeError = require('./CodeError');
const bcrypt = require('bcrypt');
const saltRounds = 13;


class Authenticator {
	static async authenticate(login, password) {
		const user = await db.get().oneOrNone(
			"SELECT id, login, password from \"user\" where login = $1",
			[login],
		);
		if (!user) {
			throw new CodeError('Пользователь не найден', 404);
		}
		if (!password === user.password) {
			throw new CodeError('Неверный пароль', 401);
		}
		Reflect.deleteProperty(user, 'password');
		const hash = await bcrypt.hash(JSON.stringify(user), saltRounds);
		return {
			identity: user,
			token: hash,
		};
	}

	static async check(user, token, saved_token) {
		if (!saved_token) {
			throw new CodeError('Пользователь не авторизован', 401);
		}
		const isSimilar = await bcrypt.compare(user, token);
		if (!isSimilar) {
			throw new CodeError('Данные клиенты переписаны', 412);
		}
	}
}

module.exports = Authenticator;