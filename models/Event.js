const BaseModel = require('../base/BaseModel');

class EventModel extends BaseModel {
	static get table() {
		return 'event'
	} ;

	constructor(data) {
		super();
		this.name = data.name;
		this.start_date = data.start_date;
		this.end_date = data.end_date;
	}
}

module.exports = EventModel;