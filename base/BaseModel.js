const db = require('../db');
const pgp = require('pg-promise')();


class BaseModel {

	static get table() {
		console.log('should be implement');
	};

	static get db() {
		return db.get()
	};

	static async list(fields) {
		if (!fields || !Object.keys(fields).length) {
			fields = '*';
		}
		return this.db.any('SELECT $1:name from $2:name', [fields, this.table]);
	}

	static async getById(id, fields) {
		if (!fields || !Object.keys(fields).length) {
			fields = '*';
		}
		return this.db.oneOrNone('SELECT $1:name from $2:name where id = $3', [fields, this.table, id]);
	}

	static async create(data) {
		return this.db.one('INSERT into $1:name values(${$2:name}) values(${$2:csv}) returning *', [this.table, data]);
	}

	static async update(condition, data) {
		const where = BaseModel._getQueryParams(condition, 'where');
		const set = BaseModel._getQueryParams(data, 'set');
		if (!set.length) {
			return;
		}
		return this.db.query('UPDATE $1:name $2:raw $3:raw returning *', [this.table, set, where]);
	}

	constructor(exist = false) {
		this.exist = exist;
		Object.defineProperty(this, 'exist', {enumerable: false});
	}

	async update(data) {
		if (!this.exist) {
			return this;
		}
		const set = BaseModel._getQueryParams(data, 'set');
		const table = this.constructor.table;
		if (!set.length) {
			return this;
		}
		return BaseModel.db.one('UPDATE $1:name $2:raw where id = $3 returning *', [table, set, this.id]);
	}

	async save() {
		if (this.exist) {
			return this.update(this);
		}

		const table = this.constructor.table;
		const {id} = await
			BaseModel.db.one('INSERT into $1:name($2:name) values($2:csv) returning id', [table, this]);
		this.id = id;
		return this;
	}

	static _getQueryParams(data, action) {
		const keysArray = Object.keys(data).filter(item => typeof data[item] !== 'undefined')
			.map(item => `${item} = $(${item})`);
		return pgp.as.format(action + ' ' + keysArray.join(', ').trim(), data);
	}
}

module.exports = BaseModel;

